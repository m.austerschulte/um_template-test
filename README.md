# um_template-test

Projekt zum Testen des Layout-Templates

## Ausführung

Starten wie die anderen map.apps Projekte mit 
````
mvn jetty:run -P watch-all,include-mapapps-deps
````

Um das Template in anderen Apps zu verwenden, muss das Projekt mit 

````
mvn clean install -P compress
````

gebaut werden. Das gebaute Projekt liegt dann im `target`-Ordner in der Datei 

````
mapapps-4-developers-1.0.0-SNAPSHOT.jar
````