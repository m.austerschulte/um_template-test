# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Changed
- sample_camera sample for sync logging added
- sample_camera small enhancements

## [4.7.1] - 2019-08-16
### Changed
- Support for map.apps 4.7.1.
- Update ct.jsregistry.version version.

## [4.7.0] - 2019-06-28
### Changed
- Support for map.apps 4.7.0.
- Update ct.jsregistry.version version.
- Update vue-template-compiler version.
- Update eslint-config-ct-prodeng version.

## [4.6.1] - 2019-04-24
### Added
- Explain `-Denv=dev`.
- Add support for local configuration of `proxy.use.rules`.

### Changed
- Support for map.apps 4.6.1.
- Update node/npm versions, add profile for dedicated npm install, use newer jetty version.

## [4.6.0] - 2019-03-01
### Changed
- Support for map.apps 4.6.0.
- Update ct-mapapps-gulp-js version.
- Use maven.home not M2_HOME.
- Update node, npm and dependencies.

### Removed
- Property `trustedServers` has been removed with property `corsEnabledServers`.

[Unreleased]: https://github.com/conterra/mapapps-4-developers/compare/4.6.0...HEAD
[4.6.1]: https://github.com/conterra/mapapps-4-developers/compare/4.6.0...4.6.1
[4.6.0]: https://github.com/conterra/mapapps-4-developers/compare/4.5.0...4.6.0