import Vue from "apprt-vue/Vue";
import Toolbar from "./Toolbar.vue";
import VueDijit from "apprt-vue/VueDijit";

export default class {
    createInstance() {
        let vm = new Vue(Toolbar);
        vm.$on("tool1-click", event => {
            // TODO
            this.tool1.click();
        });
        vm.$on("tool2-click", event => {
            // TODO
        });
        let widget = VueDijit(vm);
        return widget;
    }
}